<?php
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    include 'config.php';
    
    $msgText = null;
    $errText = null;
    
    session_start();

    if ($_POST)
    {
        if ($_POST['type'] == 'registration')
        {
            if (isset($_POST['user']) && isset($_POST['pass'])) 
            {
                try
                {
                    $pdo = new PDO("mysql:host={$config['host']};dbname={$config['dbname']};charset={$config['charset']}",$config['username'], $config['pass']);
                    
                    $sqlText = 'SELECT id FROM user WHERE login = :login ;';
                    $stmt = $pdo->prepare($sqlText);
                    $stmt->execute(array(':login' =>  "{$_POST['user']}" ));
                    $res = $stmt->fetch();
                    
                    if ( !empty($res) )
                    {
                        $msgText = 'Такой есть уже пользователь!';
                    }
                    else
                    {
                       $sqlText = "INSERT INTO user(login, password) VALUES ( :login , :pass );";
                       $stmt = $pdo->prepare($sqlText);
                       $stmt->bindParam(':login', $_POST['user']);
                       $stmt->bindParam(':pass', $_POST['pass']);
                       
                       $stmt->execute();
                       
                       $msgText = 'Вы зарегистрированы в системе!' ;
                    }
                    
                }
                catch (Exception $e)
                {
                   $errText = "Ошибка 1: " . $e->getMessage();
                }
            }
            else
            {
               $errText = ' Ошибка : Не заполнены поля для регистрации! ';     //?! не уверен что это выводить нужно когда есть "required placeholder"
            }
        }
        elseif ($_POST['type'] == 'authorization')
        {
            
            if (isset($_POST['user']) && isset($_POST['pass'])) 
            {
                try
                {
                    $pdo = new PDO("mysql:host={$config['host']};dbname={$config['dbname']};charset={$config['charset']}",$config['username'], $config['pass']);
                    
                    $sqlText = 'SELECT id FROM user WHERE login = :login and password = :pass ;';
                    $stmt = $pdo->prepare($sqlText);
                    $stmt->execute(array(':login' =>  "{$_POST['user']}" , ':pass' =>  "{$_POST['pass']}" ));
                    $res = $stmt->fetch();
                    
                    if ( !empty($res) )
                    {  
                        $_SESSION['user_id'] = $res['id'];
                        $_SESSION['login'] = $_POST['user'];
                        $_SESSION['pass'] = $_POST['pass'];
                        $ref = 'Location: tasks.php';  
                        header($ref);
                    }
                    else
                    {
                       $errText = 'Не верно заполнены поля логин/пароль';
                    }
                }
                catch (Exception $e)
                {
                   $errText = "Ошибка 2: " . $e->getMessage();
                }
            }
            else
            {
               $errText =' Ошибка : Не заполнены поля! ';     //?! не уверен что это выводить нужно когда есть "required placeholder"
            }
        }
    }
    else // для выхода пользователя из приложения
    {
        $_SESSION = array();
        if ( session_id() != "" || isset($_COOKIE[session_name()]) )
        {
            setcookie(session_name(), '', time()-2592000, '/');
            session_destroy();
        }
    }

?>

<!DOCTYPE html>
<html lang="ru">
  <head>
      <meta content="text/html; charset=utf-8" />
      <title>Задание к лекции 4.3 </title>
      <link rel="stylesheet" href="style.css">
  </head>
  <body>
     <p class="error"> <?=$errText?> </p>
     <p> <?=$msgText?> </p>
     
     <h4>Авторизация пользователя : </h4>
     <form action="" method="POST">
          <p> Имя <input  name="user" type="text" value="" required placeholder="..." /></p>
          <input  name="type" type="hidden" value="authorization" />
          <p> Пароль <input  name="pass" type="password" value="" required placeholder="..." /></p>
         <input type="submit" value="Авторизация">
     </form>
     
     <h4>Регистрация пользователя : </h4>
     <form action="" method="POST">
          <p> Имя <input  name="user" type="text" value="" required placeholder="..." /></p>
          <input  name="type" type="hidden" value="registration" />
          <p> Пароль <input  name="pass" type="password" value="" required placeholder="..." /></p>
         <input type="submit" value="Регистрация">
     </form>
     
  </body>
</html>
