<?php
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    
    include 'config.php';
    session_start();
    
    $msgText = null;
    $errText = null;
    
    if (isset($_SESSION['user_id']))
    {   
        try
        {
            $pdo = new PDO("mysql:host={$config['host']};dbname={$config['dbname']};charset={$config['charset']}",$config['username'], $config['pass']);
        }
        catch (Exception $e)
        {
           $errText = "Ошибка 3: " . $e->getMessage();
           exit;
        }
         
        if( isset($_GET['cmd']) )
        {     
            if( ($_GET['cmd'] == 'add') )
            {
                if (isset($_POST['description']) && isset($_POST['isdone']) )
                { 
                    $sqlText = 'INSERT INTO task (user_id, assigned_user_id, description, is_done, date_added) VALUES (:user_id, :assigned_user_id, :descr, :isdone, NOW( ))';
                    $stmt = $pdo->prepare($sqlText);
                    
                    $stmt->bindParam(':user_id' , $_SESSION['user_id']);
                    $stmt->bindParam(':assigned_user_id' , $_SESSION['user_id']);
                    $stmt->bindParam(':descr' , $_POST['description']);
                    $stmt->bindParam(':isdone' , $_POST['isdone']);
                    $stmt->execute();
                }
                else
                {
                    echo '
                    <h3>Новая задача : </h3>
                    <form method="POST" action="/tasks.php?cmd=add" >
                        <p> Описание задачи <input  name="description" type="text" value="" size="150" required placeholder="..." /></p>
                        <p> Выполнено (1/0) <input  name="isdone" type="text" value="0" size="1" required placeholder="..." /></p>
                        <input type="submit" value="Создать новую задачу">
                    </form>';
                    exit;
                }
            }
            elseif( ($_GET['cmd'] == 'delTask') )
            {
                $sqlText = "DELETE FROM task WHERE user_id=${_SESSION['user_id']} and id=${_GET['id']};"; // как здесь сделать запрос безопасным ?! 
                $pdo->exec($sqlText);
            }
            elseif( ($_GET['cmd'] == 'endTask') )
            {
                $sqlText = "UPDATE task set is_done = not is_done WHERE user_id = :user_id and id = :id;";
                $stmt = $pdo->prepare($sqlText);
                $stmt->bindValue(':user_id', $_SESSION['user_id']);
                $stmt->bindValue(':id', $_GET['id']);
                $stmt->execute();
                 
                $msgText = $sqlText;   
            }
        }
        
        $countTask = countTasks($pdo, $_SESSION['user_id'], $_SESSION['user_id']);  
        $tasks = loadTasks($pdo, $_SESSION['user_id']);
        $users = loadUser($pdo);
		
    }
  else
  {   
      header($_SERVER['SERVER_PROTOCOL'] . ' 403 Unautorized');
      exit('<h1>403 Unautorized</h1><p>Перейти к <a href="index.php">форме авторизации</a></p>');   
  }
    
//--------------------------------------------------------------------------------
    function loadTasks($pdo, $user_id)
    {    
        $tasks = array();
        if (!empty($user_id))
        {
            $sqlText = 'SELECT t1.*, t2.login FROM task as t1 JOIN user as t2 ON t1.user_id = t2.id AND user_id = :user_id ORDER BY date_added ;';
            $stmt = $pdo->prepare($sqlText);
            $stmt->execute(array(':user_id' => "$user_id" ));
            $tasks = $stmt->fetchAll(); 
        }
        return $tasks;
    }

    function loadUser($pdo)
    {    
        $users = array();
        $sqlText = 'SELECT id, login FROM user ;';
        $stmt = $pdo->prepare($sqlText);
        $stmt->execute();
        $users = $stmt->fetchAll();
        
        return $users;
    }
    
    function countTasks($pdo, $user_id, $assigned_user_id )
    {
        $count = 0;
        if ( !empty($user_id) && !empty($assigned_user_id) )
        {
            $sqlText = 'SELECT count(*) as count FROM task WHERE user_id = :user_id OR assigned_user_id = :user_id ';
            $stmt = $pdo->prepare($sqlText);
            $stmt->execute(array(':user_id' =>  "$user_id", ':assigned_user_id' => "$assigned_user_id" ));
            
            $count = $stmt->fetch();
        }
        return $count['count'];
    }

?>

<!DOCTYPE html>
<html lang="ru">
  <head>
      <meta content="text/html; charset=utf-8" />
      <title>Задание к лекции 4.3 </title>
      <link rel="stylesheet" href="style.css">
  </head>
  <body>
  
     <p class="error"> <?=$errText?> </p>
     <p> <?=$msgText?> </p>
     <h4>Вывод списка ваших дел</h4>
     
     <table >
        <tr>
           <th> описание задачи</th>
           <th> дата добавления </th>
		   <th> выполнено </th>
           <th> удаление </th>
           <th> делегировать </th>
           <th> автор задачи </th>
           
        </tr>
       <?php foreach ($tasks as $ind => $task) : ?>
        <tr>
            <td> <?php echo $task['description'] ?> </td>
            <td> <?php echo $task['date_added'] ?> </td>
			<td> <a href="/tasks.php?cmd=endTask&id=<?=$task['id']?>"> <?php echo $task['is_done'] ? 'Да' : 'Нет' ?> </a> </td>
			
            <td> <a href="/tasks.php?cmd=delTask&id=<?=$task['id']?>"> Удалить </a> </td>
			
            <td> 
            <form method= "POST" >
                <input name="setTask" type="hidden" value ="<?=$task['id']?>">
                <select name="assigned_user_id">
                <?php foreach ($users as $user) : ?>
                   <option> <?=$user['login']?> </option>
                <?php endforeach; ?>
                </select>
                <input value="Делегировать" type="submit"/>
            </form>
            </td>
            
            <td> <?=$task['login']?> </td>
            
         </tr>
       <?php endforeach; ?>
     </table>
     <p> Кол-во всего заданий : <?=$countTask?></p>
     <p><a href="tasks.php?cmd=add"> Добавить задачу </a></p>
	 <p><a href="index.php"> Выход </a></p>
	 
  </body>
</html>    
